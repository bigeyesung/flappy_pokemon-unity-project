﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	//The force which is added when the player jumps
	//This can be changed in the Inspector window
	public Vector2 jumpForce = new Vector2(0,300);
	public Rigidbody2D rbody;

	// Use this for initialization
	void Start () {
		rbody = GetComponent<Rigidbody2D>();

	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyUp ("space")) {
			rbody.velocity= Vector2.zero; //(0,0)
			rbody.AddForce(jumpForce);

		}


		//Die by being off screen
		Vector2 screenPosition = Camera.main.WorldToScreenPoint (transform.position);
		if (screenPosition.y > Screen.height || screenPosition.y < 0) {
		
			Die();
		}


	}
	//Die by Collision (is called whenever there is a collision detected between two GameObjects that have Collider 2D components.)
	void OnCollisionEnter2D(Collision2D other){
		
		Die();
	}
	
	void Die(){
		Application.LoadLevel (Application.loadedLevel);
	}

}
