﻿using UnityEngine;
using System.Collections;

public class Generate : MonoBehaviour {

	public GameObject rocks;
	int score = 0;
	// Use this for initialization
	void Start () {

		InvokeRepeating ("CreateObstacle", 1f,1.5f);
	}
	
	// Update is called once per frame


	void OnGUI(){



		GUILayout.BeginArea (new Rect((Screen.width/2)-50, (Screen.height/2) , 100, 100));

		GUI.color = Color.black;
		GUILayout.Label (" You have scored: " + score.ToString());

		GUILayout.EndArea ();

	
	}

	void CreateObstacle(){

		Instantiate (rocks);
		score++;

	}
}
