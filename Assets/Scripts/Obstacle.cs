﻿using UnityEngine;
using System.Collections;

public class Obstacle : MonoBehaviour {
	public Vector2 velocity = new Vector2 (-4,0);
	public float range = 4;
	public Rigidbody2D rbody;
	// Use this for initialization
	void Start () {
		rbody = GetComponent<Rigidbody2D>();
		rbody.velocity = velocity;
		transform.position = new Vector3 (transform.position.x, transform.position.y - range * Random.value, transform.position.z);

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
